package voipot.jnu.conoz.com.voipotguide.ui.message.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import voipot.jnu.conoz.com.voipotguide.databinding.ItemMessageLeftBinding
import voipot.jnu.conoz.com.voipotguide.databinding.ItemMessageRightBinding
import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity

class MessageListAdapter(private val lifecycleOwner: LifecycleOwner)
    : ListAdapter<MessageEntity, MyViewHolder>(
        object : DiffUtil.ItemCallback<MessageEntity>() {
            override fun areItemsTheSame(oldItem: MessageEntity, newItem: MessageEntity): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MessageEntity, newItem: MessageEntity): Boolean {
                return (oldItem.message == newItem.message) && (oldItem.name == oldItem.name) && (oldItem.time == oldItem.time)
            }
        }) {
    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).name) {
            "가이드" -> 1
            else -> 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return when (viewType) {
            1 -> RightViewHolder(ItemMessageRightBinding.inflate(LayoutInflater.from(parent.context), parent, false), lifecycleOwner)

            2 -> LeftViewHolder(ItemMessageLeftBinding.inflate(LayoutInflater.from(parent.context), parent, false), lifecycleOwner)

            else -> throw RuntimeException("MessageRecyclerAdapter의 viewType 설정이 안 됐습니다")
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindItem(MessageItem(getItem(position)))
    }
}

class MessageItem(val entity: MessageEntity) {
    val name = entity.name
    val message = entity.message
    val time = entity.time!!.split("-", " ", ":").run {
        "${get(size - 3)}:${get(size - 2)}"
    }
}

abstract class MyViewHolder(binding: ViewDataBinding, lifecycleOwner: LifecycleOwner) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bindItem(item: MessageItem)

    init {
        binding.lifecycleOwner = lifecycleOwner
        binding.executePendingBindings()
    }
}

class LeftViewHolder(private val binding: ItemMessageLeftBinding,
                     lifecycleOwner: LifecycleOwner) : MyViewHolder(binding, lifecycleOwner) {
    override fun bindItem(item: MessageItem) {
        binding.messageItem = item
    }
}

class RightViewHolder(private val binding: ItemMessageRightBinding,
                      lifecycleOwner: LifecycleOwner) : MyViewHolder(binding, lifecycleOwner) {
    override fun bindItem(item: MessageItem) {
        binding.messageItem = item
    }
}