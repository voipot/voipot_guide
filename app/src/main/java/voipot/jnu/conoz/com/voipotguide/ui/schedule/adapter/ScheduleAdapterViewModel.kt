package voipot.jnu.conoz.com.voipotguide.ui.schedule.adapter

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import voipot.jnu.conoz.com.voipotguide.application.CommonApp
import voipot.jnu.conoz.com.voipotguide.room.Time
import voipot.jnu.conoz.com.voipotguide.room.entity.ScheduleEntity
import voipot.jnu.conoz.com.voipotguide.transport.ScheduleSendTCP
import voipot.jnu.conoz.com.voipotguide.util.IScheduleManager
import javax.inject.Inject

class ScheduleAdapterViewModel @Inject constructor(private val scheduleManager: IScheduleManager)
    : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    private val _scheduleData = MutableLiveData<MutableList<ScheduleEntity>>()
    val scheduleData: LiveData<MutableList<ScheduleEntity>>
        get() = _scheduleData

    val hasSchedule: LiveData<Boolean>
    val titleText = MutableLiveData<String>()
    val memoText = MutableLiveData<String>()

    private val _doneClick = MutableLiveData<Boolean>()
    val doneClick: LiveData<Boolean>
        get() = _doneClick

    init {
        hasSchedule = Transformations.map(_scheduleData) {
            it.size != 0
        }

        _scheduleData.value = ArrayList()

        Observable.just(scheduleManager)
                .subscribeOn(Schedulers.io())
                .map {
                    it.getAll()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.isNotEmpty()) {
                        changeScheduleList(it)
                    }
                }.also {
                    compositeDisposable.add(it)
                }
    }

    fun removeSchedule(position: Int) {
        _scheduleData.value = _scheduleData.value!!.apply { removeAt(position) }
    }

    fun removeSchedule(item: ScheduleEntity) {
        _scheduleData.value = _scheduleData.value!!.apply { remove(item) }
    }

    fun addSchedule(position: Int, schedule: ScheduleEntity) {
        _scheduleData.value = _scheduleData.value!!.apply { add(position, schedule) }
    }

    fun changeScheduleList(scheduleList: List<ScheduleEntity>) {
        _scheduleData.value = ArrayList(scheduleList)
    }

    fun onAddScheduleClick(clickedView: View) {
        _doneClick.value = true

        val entity = ScheduleEntity().apply {
            time = Time(0, 0)
            title = titleText.value!!
            memo = memoText.value!!
        }

        titleText.value = ""
        memoText.value = ""

        Observable.just(scheduleManager)
                .subscribeOn(Schedulers.io())
                .map {
                    entity.id = it.add(entity)
                    scheduleManager.getPosition(entity)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    addSchedule(it, entity)
                    CommonApp.clientList.forEach { info ->
                        ScheduleSendTCP().sendSchedule(info.ip, entity)
                    }
                }.also {
                    compositeDisposable.add(it)
                }
    }

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.clear()
    }
}