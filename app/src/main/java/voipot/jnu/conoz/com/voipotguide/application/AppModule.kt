package voipot.jnu.conoz.com.voipotguide.application

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import voipot.jnu.conoz.com.voipotguide.ui.ViewModelProviderFactory
import voipot.jnu.conoz.com.voipotguide.util.DataManager
import voipot.jnu.conoz.com.voipotguide.util.IDataManager
import javax.inject.Singleton

@Module
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun bindDataManager(dataManager: DataManager): IDataManager

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @Singleton
    abstract fun bindCotext(application: Application): Context
}