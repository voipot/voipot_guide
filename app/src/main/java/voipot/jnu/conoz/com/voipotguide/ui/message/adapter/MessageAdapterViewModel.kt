package voipot.jnu.conoz.com.voipotguide.ui.message.adapter

import android.widget.AbsListView
import androidx.databinding.adapters.AbsListViewBindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.doAsyncResult
import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity
import voipot.jnu.conoz.com.voipotguide.util.IDataManager
import voipot.jnu.conoz.com.voipotguide.util.IMessageManager
import javax.inject.Inject

class MessageAdapterViewModel @Inject constructor(private val messageManager: IMessageManager) : ViewModel() {
    private val _messageData = MutableLiveData<List<MessageEntity>>()
    val messageData: LiveData<List<MessageEntity>>
        get() = _messageData

    private val _messagePage = MutableLiveData<Int>()
    val messagePage: LiveData<Int>
        get() = _messagePage

    init {
        doAsyncResult {
            messageManager.getMessage(1)
        }.let {
            _messageData.value = it.get()
        }

        _messagePage.value = 1
    }

    val onScroll = AbsListViewBindingAdapter.OnScroll { _, firstVisibleItem, visibleItemCount, totalItemCount ->
        val lastVisiblePosition = firstVisibleItem + visibleItemCount

        if (lastVisiblePosition == totalItemCount) {
            _messagePage.value!!.plus(1)
            _messageData.value = ArrayList(_messageData.value).apply { addAll(messageManager.getMessage(messagePage.value!!)) }
        }
    }

    val onScrollListener = object :RecyclerView.OnScrollListener(){
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val lastVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            val count = recyclerView.adapter!!.itemCount

            if (lastVisiblePosition == count) {
                _messagePage.value!!.plus(1)
                _messageData.value = ArrayList(_messageData.value).apply { addAll(messageManager.getMessage(messagePage.value!!)) }
            }
        }
    }

    /*fun onScrollChange(recyclerView: RecyclerView) {

        val lastVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        val count = recyclerView.adapter!!.itemCount

        if (lastVisiblePosition == count) {
            _messagePage.value!!.plus(1)
            _messageData.value = ArrayList(_messageData.value).apply { addAll(messageManager.getMessage(messagePage.value!!)) }
        }
    }*/
}