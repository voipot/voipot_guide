package voipot.jnu.conoz.com.voipotguide.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import voipot.jnu.conoz.com.voipotguide.application.CommonApp
import java.io.*
import java.net.InetAddress
import java.net.Socket

class MessageSendWorker(context: Context, workerParameters: WorkerParameters)
    : Worker(context, workerParameters) {
    override fun doWork(): Result {
        val ip = inputData.getString("ip")
        val name = inputData.getString("name")
        val time = inputData.getString("time")
        val message = inputData.getString("message")

        val inetAddress = InetAddress.getByName(ip)
        val socket = Socket(inetAddress, CommonApp.PORT_MESSAGE)
        try {
            println("IP : $ip")

            val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
            out.println(name!! + "|," + time + "|," + message!!)

            BufferedReader(InputStreamReader(socket.getInputStream())).let {
                val returnedMsg = it.readLine()
                println("returnedMsg : $returnedMsg")
            }
//                onSendListener(true)
        } catch (e: IOException) {
            e.printStackTrace()
            System.err.println("IOException : " + e.message)
//                onSendListener(false)
        } finally {
            socket.close()
        }

        return Result.success()

    }

}