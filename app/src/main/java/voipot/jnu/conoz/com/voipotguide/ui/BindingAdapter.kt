package voipot.jnu.conoz.com.voipotguide.ui

import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2

@BindingAdapter("currentPage")
fun ViewPager.setCurrentPage(currentPage: Int) {
    setCurrentItem(currentPage, true)
}

@InverseBindingAdapter(attribute = "currentPage", event = "onPageSelect")
fun ViewPager.getCurrentPage(): Int {
    return currentItem
}

@BindingAdapter("onPageSelect")
fun ViewPager.onPageChange(pageChange: InverseBindingListener) {
    addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            pageChange.onChange()
        }
    })
}

@BindingAdapter("currentPage")
fun ViewPager2.setCurrentPage(currentPage: Int) = setCurrentItem(currentPage, true)

@InverseBindingAdapter(attribute = "currentPage", event = "onPageSelect")
fun ViewPager2.getCurrentPage() = currentItem

@BindingAdapter("onPageChangeCallback")
fun ViewPager2.onPageChangeCallback(pageChangeCallback: ViewPager2.OnPageChangeCallback) =
        registerOnPageChangeCallback(pageChangeCallback)

@BindingAdapter("onPageSelect")
fun ViewPager2.onPageChange(pageChange: InverseBindingListener) =
        registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                pageChange.onChange()
            }
        })

@BindingAdapter("data")
fun <T> RecyclerView.setData(data: List<T>) {
    @Suppress("UNCHECKED_CAST")
    (adapter as ListAdapter<T, *>).submitList(data)
}

@BindingAdapter("onScrollListener")
fun RecyclerView.onScrollListener(onScrollListener: RecyclerView.OnScrollListener){
    addOnScrollListener(onScrollListener)
}

@BindingAdapter("imgRes")
fun setImgRes(imgBtn: ImageButton, resId: Int) = imgBtn.setBackgroundResource(resId)

@BindingAdapter("visibleOrGone")
fun setVisibleOrGone(viewGroup: ViewGroup, visible: Boolean) {
    viewGroup.visibility = if (visible) ViewGroup.VISIBLE else ViewGroup.GONE
}

@BindingAdapter("visibleOrGone")
fun setVisibleOrGone(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}