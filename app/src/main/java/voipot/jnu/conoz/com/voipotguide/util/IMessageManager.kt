package voipot.jnu.conoz.com.voipotguide.util

import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity


interface IMessageManager {
    fun addMessage(entity: MessageEntity)
    fun getMessage(page: Int): List<MessageEntity>
    fun readUpdate()
}
