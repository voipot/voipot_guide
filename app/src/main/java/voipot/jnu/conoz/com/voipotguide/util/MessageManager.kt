package voipot.jnu.conoz.com.voipotguide.util

import io.reactivex.Observable
import voipot.jnu.conoz.com.voipotguide.room.AppDatabase
import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity

class MessageManager(db: AppDatabase) : IMessageManager {
    companion object{
        private val LIMIT_NUM = 50
    }
    private val messageDao = db.messageDAO()
    override fun addMessage(entity: MessageEntity) {
        messageDao.insert(entity)
    }

    override fun getMessage(page: Int): ArrayList<MessageEntity> {
        val s = messageDao.getCount()

        val minimumId = s - page * LIMIT_NUM
        val maximumId = minimumId + LIMIT_NUM

        readUpdate()

        return ArrayList(messageDao.findAllByIdRange(minimumId, maximumId))
    }

    override fun readUpdate() {
        messageDao.updateRead()
    }
}
