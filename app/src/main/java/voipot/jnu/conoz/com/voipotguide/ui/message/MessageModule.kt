package voipot.jnu.conoz.com.voipotguide.ui.message

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import voipot.jnu.conoz.com.voipotguide.ui.ViewModelKey
import voipot.jnu.conoz.com.voipotguide.ui.message.adapter.MessageAdapterViewModel
import voipot.jnu.conoz.com.voipotguide.ui.schedule.ScheduleViewModel
import voipot.jnu.conoz.com.voipotguide.util.DataManager
import voipot.jnu.conoz.com.voipotguide.util.IDataManager
import voipot.jnu.conoz.com.voipotguide.util.IMessageManager

@Module
abstract class MessageModule{
    @Binds
    abstract fun bindMessageManager(dataManager: IDataManager): IMessageManager

    @Binds
    @IntoMap
    @ViewModelKey(MessageViewModel::class)
    abstract fun bindMessageViewModel(messageViewModel: MessageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MessageAdapterViewModel::class)
    abstract fun bindMessageAdapterViewModel(messageAdapterViewModel: MessageAdapterViewModel): ViewModel
}