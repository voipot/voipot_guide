package voipot.jnu.conoz.com.voipotguide.transport

import voipot.jnu.conoz.com.voipotguide.application.CommonApp
import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity
import java.io.*
import java.net.InetAddress
import java.net.Socket

class MessageSendTCP {
    fun sendMessage(ip: String, entity: MessageEntity) {
        Thread(Runnable {
            val inetAddress = InetAddress.getByName(ip)
            val socket = Socket(inetAddress, CommonApp.PORT_MESSAGE)
            try {
                println("IP : $ip")

                val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
                out.println(entity.name!! + "|," + entity.time + "|," + entity.message!!)

                BufferedReader(InputStreamReader(socket.getInputStream())).let {
                    val returnedMsg = it.readLine()
                    println("returnedMsg : $returnedMsg")
                }
//                onSendListener(true)
            } catch (e: IOException) {
                e.printStackTrace()
                System.err.println("IOException : " + e.message)
//                onSendListener(false)
            } finally {
                socket.close()
            }
        }).start()
    }
}