package voipot.jnu.conoz.com.voipotguide.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "message")
class MessageEntity(
        @PrimaryKey(autoGenerate = true) var id: Int?,
        var name: String?,
        var time: String?,
        var message: String?,
        var isRead: Boolean?) {
    constructor() : this(null, null, null, null, null)
}


//var id : Int? = null
//var name: String? = null
//var time: String? = null
//var message: String? = null
//var isRead : Boolean? = null
//var isValid: Boolean = false
//    get() = !(name.isNullOrEmpty() || time.isNullOrEmpty() || message.isNullOrEmpty())