package voipot.jnu.conoz.com.voipotguide.ui.message


import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_message.*
import voipot.jnu.conoz.com.voipotguide.R
import voipot.jnu.conoz.com.voipotguide.databinding.FragmentMessageBinding
import voipot.jnu.conoz.com.voipotguide.ui.base.BaseFragment
import voipot.jnu.conoz.com.voipotguide.ui.message.adapter.MessageAdapterViewModel
import voipot.jnu.conoz.com.voipotguide.ui.message.adapter.MessageListAdapter
import voipot.jnu.conoz.com.voipotguide.util.IMessageManager
import voipot.jnu.conoz.com.voipotguide.util.viewModelProvider
import javax.inject.Inject

class MessageFragment @Inject constructor() : BaseFragment<FragmentMessageBinding>() {
    override val layoutResourceId: Int = R.layout.fragment_message

    private lateinit var messageViewModel: MessageViewModel
    private lateinit var messageAdapterViewModel: MessageAdapterViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val messageListAdapter by lazy { MessageListAdapter(this) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        messageViewModel = viewModelProvider(viewModelFactory)
        messageAdapterViewModel = viewModelProvider(viewModelFactory)

        with(viewDataBinding) {
            messageViewModel = this@MessageFragment.messageViewModel
            messageAdapterViewModel = this@MessageFragment.messageAdapterViewModel
        }

        initRecyclerView()
        observe()
        //        btnSend.setOnClickListener {
//            if (editMessage.text.isNotEmpty()) {
//                editMessage.text.toString().let { editMessage ->
//                    //브로드캐스트로 할 수도 있으니 나중에 한번더 체크
//                    val entity = MessageEntity().apply {
//                        name = "가이드"
//                        message = editMessage
//                        time = CommonApp.getCurrentTime()
//                        isRead = true
//                    }
//
//                    doAsync {
//                        messageManager.addMessage(entity)
//
//                        uiThread {
//                            addMessage(entity)
//                        }
//                    }
//
//                    val baseData = Data.Builder()
//                            .putString("name", entity.name)
//                            .putString("time", entity.time)
//                            .putString("message", entity.message)
//                            .build()
//
//                    CommonApp.clientList.map {
//                        OneTimeWorkRequestBuilder<MessageSendWorker>()
//                                .setInputData(
//                                        Data.Builder()
//                                                .putString("ip", it.ip)
//                                                .putAll(baseData).build()
//                                ).build()
//                    }.let {
//                        WorkManager.getInstance(activity!!).enqueue(it)
//                    }
//
//                }
//                editMessage.text.clear()
//            }
//

    }

    private fun initRecyclerView() {
        recyclerMessage.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, true)
        recyclerMessage.adapter = messageListAdapter
//        recyclerMessage.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//
//                val lastVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
//                val count = recyclerView.adapter!!.itemCount
//
//                if (lastVisiblePosition == count) {
//                    messagePage++
//                    (recyclerView.adapter as MessageListAdapter).addOldMessages(messageManager.getMessage(messagePage))
//                }
//            }
//        })
    }

    private fun observe() {
//        messageViewModel.apply {
//            sendMessage.observe(this@MessageFragment) {
//                if (it) {
//
//                }
//            }
//        }
    }
}
