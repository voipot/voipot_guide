package voipot.jnu.conoz.com.voipotguide.ui.message

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import voipot.jnu.conoz.com.voipotguide.application.CommonApp
import voipot.jnu.conoz.com.voipotguide.room.entity.MessageEntity
import voipot.jnu.conoz.com.voipotguide.util.IDataManager
import voipot.jnu.conoz.com.voipotguide.util.IMessageManager
import voipot.jnu.conoz.com.voipotguide.worker.MessageSendWorker
import javax.inject.Inject

class MessageViewModel @Inject constructor(private val messageManager: IDataManager, app: Application) : AndroidViewModel(app) {
    val message = MutableLiveData<String>()

    private val _sendMessage = MutableLiveData<Boolean>()
    val sendMessage: LiveData<Boolean>
        get() = _sendMessage


    init {
        _sendMessage.value = false
    }

    fun onSendClick(clickedView: View) {
        if (message.value!!.isNotBlank()) {
            message.value.let { editMessage ->
                //브로드캐스트로 할 수도 있으니 나중에 한번더 체크
                val entity = MessageEntity().apply {
                    name = "가이드"
                    message = editMessage
                    time = CommonApp.getCurrentTime()
                    isRead = true
                }

                doAsync {
                    messageManager.addMessage(entity)

                    uiThread {
                        _sendMessage.value = true
                    }
                }

                val baseData = Data.Builder()
                        .putString("name", entity.name)
                        .putString("time", entity.time)
                        .putString("message", entity.message)
                        .build()

                CommonApp.clientList.map {
                    OneTimeWorkRequestBuilder<MessageSendWorker>()
                            .setInputData(
                                    Data.Builder()
                                            .putString("ip", it.ip)
                                            .putAll(baseData).build()
                            ).build()
                }.let {
                    WorkManager.getInstance(getApplication()).enqueue(it)
                }

            }
            message.value = ""
        }
    }
}