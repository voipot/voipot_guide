package voipot.jnu.conoz.com.voipotguide.application

import dagger.Module
import dagger.android.ContributesAndroidInjector
import voipot.jnu.conoz.com.voipotguide.ui.login.LoginFragmentProvider
import voipot.jnu.conoz.com.voipotguide.ui.main.MainActivity
import voipot.jnu.conoz.com.voipotguide.ui.main.MainModule
import voipot.jnu.conoz.com.voipotguide.ui.message.MessageFragmentProvider
import voipot.jnu.conoz.com.voipotguide.ui.schedule.ScheduleFragmentProvider

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [MainModule::class,
        LoginFragmentProvider::class,
        MessageFragmentProvider::class,
        ScheduleFragmentProvider::class])
    abstract fun bindMainMvvmActivity(): MainActivity
}