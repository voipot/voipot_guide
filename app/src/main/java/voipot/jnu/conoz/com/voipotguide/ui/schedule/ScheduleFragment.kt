package voipot.jnu.conoz.com.voipotguide.ui.schedule

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import kotlinx.android.synthetic.main.fragment_schedule.*
import voipot.jnu.conoz.com.voipotguide.R
import voipot.jnu.conoz.com.voipotguide.databinding.DialogWriteScheduleBinding
import voipot.jnu.conoz.com.voipotguide.databinding.FragmentScheduleBinding
import voipot.jnu.conoz.com.voipotguide.ui.base.BaseFragment
import voipot.jnu.conoz.com.voipotguide.ui.main.SharedPagerViewModel
import voipot.jnu.conoz.com.voipotguide.ui.schedule.adapter.ModifyScheduleAdapter
import voipot.jnu.conoz.com.voipotguide.ui.schedule.adapter.ScheduleAdapterViewModel
import voipot.jnu.conoz.com.voipotguide.ui.schedule.adapter.ScheduleListAdapter
import voipot.jnu.conoz.com.voipotguide.ui.schedule.adapter.TextScheduleAdapter
import voipot.jnu.conoz.com.voipotguide.util.activityViewModelProvider
import voipot.jnu.conoz.com.voipotguide.util.viewModelProvider
import javax.inject.Inject

class ScheduleFragment @Inject constructor() : BaseFragment<FragmentScheduleBinding>() {
    override val layoutResourceId: Int = R.layout.fragment_schedule

    private lateinit var scheduleVM: ScheduleViewModel
    private lateinit var sharedPagerVM: SharedPagerViewModel
    private lateinit var scheduleAdapterVM: ScheduleAdapterViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        sharedPagerVM = activityViewModelProvider(viewModelFactory)
        scheduleVM = viewModelProvider(viewModelFactory)
        scheduleAdapterVM = viewModelProvider(viewModelFactory)

        with(viewDataBinding) {
            scheduleViewModel = this@ScheduleFragment.scheduleVM
            sharedPagerViewModel = this@ScheduleFragment.sharedPagerVM
            scheduleAdapterViewModel = this@ScheduleFragment.scheduleAdapterVM
            lifecycleOwner = this@ScheduleFragment
        }

        initDialog()
        initRecyclerView()
        observe()
    }

    private lateinit var dialog: DialogPlus
    private fun initDialog() {
        val dwsb = DataBindingUtil.inflate<DialogWriteScheduleBinding>(
                LayoutInflater.from(activity), R.layout.dialog_write_schedule, null, false
        ).apply {
            scheduleAdapterViewModel = scheduleAdapterVM
            lifecycleOwner = this@ScheduleFragment
        }

        dialog = DialogPlus.newDialog(activity).apply {
            setContentHolder(ViewHolder(dwsb!!.root))

            isExpanded = false
            isCancelable = true
            setGravity(Gravity.BOTTOM)
            contentBackgroundResource = android.R.color.transparent
        }.create()
    }

    private fun initRecyclerView() {
        scheduleRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        scheduleRecycler.adapter = TextScheduleAdapter(this)
    }

    private fun observe() {
        sharedPagerVM.apply {
            isModifyingSchedule.observe(this@ScheduleFragment, Observer {
                val adapter: ScheduleListAdapter<*> = if (it) {
                    sharedPagerVM.setExpand(true)
                    ModifyScheduleAdapter(activity!!, scheduleAdapterVM)
                } else TextScheduleAdapter(this@ScheduleFragment)


                scheduleRecycler.adapter = adapter

                if (scheduleAdapterVM.scheduleData.value.isNullOrEmpty()) {
                    ArrayList()
                } else {
                    ArrayList(scheduleAdapterVM.scheduleData.value)
                }.let { list ->
                    adapter.submitList(list)
                }
            })
        }

        scheduleVM.apply {
            addScheduleClick.observe(this@ScheduleFragment, Observer {
                if (it) {
                    dialog.show()
                }
            })
        }

        scheduleAdapterVM.apply {
            doneClick.observe(this@ScheduleFragment, Observer {
                if (it) {
                    dialog.dismiss()
                }
            })
        }
    }
}