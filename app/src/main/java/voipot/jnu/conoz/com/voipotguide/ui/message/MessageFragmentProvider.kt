package voipot.jnu.conoz.com.voipotguide.ui.message

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MessageFragmentProvider{
    @ContributesAndroidInjector(modules = [MessageModule::class])
    abstract fun bindMessageFragment(): MessageFragment
}